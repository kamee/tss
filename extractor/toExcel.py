import csv

class Excel:
    def to_excel(self, data):
        filename = "user_details.csv"
        header = ("uid", "name", "status", "email")
        with open(filename, "w", newline="") as csvfile:
            users = csv.writer(csvfile)
            users.writerow(header)
            for x in data:
                users.writerow(x)
