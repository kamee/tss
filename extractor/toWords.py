class ToWords:
    ONES = [
            None, 'մեկ', 'երկու', 'երեք', 'չորս',
            'հինգ', 'վեց', 'յոթ', 'ութ', 'ինն'
            ]
 
    TEENS = [
            'տասն', 'տասնմեկ', 'տասներկու', 'տասներեք', 'տասնչորս',
            'տասնհինգ', 'տասնվեց', 'տասնյոթ', 'տասնութ', 'տասնինը'
            ]
 
    TENS = [
            None, None, 'քսան', 'երեսուն', 'քառասուն',
            'հիսուն', 'վաթսուն', 'յոթանասուն', 'ութսուն', 'իննսուն'
            ]
 
    POWERS_OF_1000 = [
            None, 'հազար', 'միլիոն', 'միլիարդ'
            ]
 
 
    def number_to_chunks(self, number):
        while number:
            number, n = divmod(number, 1000)
            yield n
            
    def chunk_to_words(self, chunk, scale):
        assert 0 <= chunk < 1000
        hundreds, tens, ones = chunk // 100, chunk // 10 % 10, chunk % 10
            
        if hundreds:
            yield self.ONES[hundreds]
            yield 'հարյուր'
                                                                                                                                               
        if tens == 1:
            yield self.TEENS[ones]
        else:
            if tens:
                yield self.TENS[tens]
            
        if ones:
            yield self.ONES[ones]
                                                                                                                                                                                                              
        if chunk and scale:
            yield self.POWERS_OF_1000[scale]
                                                                                                                                                                                                                                    
    def number_to_words(self, number):
        chunks = [
                ' '.join(self.chunk_to_words(chunk, i))
                for i, chunk in enumerate(self.number_to_chunks(number))
                ]
        return ' '.join(c for c in chunks[::-1] if c) or 'զրո'

#t = ToWords()
#print(t.number_to_words(3))
