import os
import urllib.request, json 
import requests
import time
from mastodon import Mastodon


mastodon = Mastodon(
        access_token = '',
        api_base_url = ""
        );


subreddit = "https://www.reddit.com/r/HistoryMemes.json"

def get():
    with urllib.request.urlopen(subreddit) as url:
        data = json.loads(url.read().decode())
        red_data = data['data']

        for i in range(len(red_data['children'])):
            img_url = red_data['children'][i]['data']['url_overridden_by_dest']
            selftext = red_data['children'][i]['data']['selftext']
            title = red_data['children'][i]["data"]['title']
            post_id = red_data['children'][i]['data']['id']
            if tooted(post_id) == False:
                print("call tooted")
                tooter(title, selftext, img_url, post_id)
            time.sleep(500.0)

IMAGE_DIR = "img"

def image(img_url):
    if not os.path.exists(IMAGE_DIR):
        os.makedirs(IMAGE_DIR)


    file_name = os.path.basename(urllib.parse.urlsplit(img_url).path)
    img_path = IMAGE_DIR + '/' + file_name
    print('[bot] Downloading image at URL ' + img_url + ' to ' + img_path)
    resp = requests.get(img_url, stream=True)
    if resp.status_code == 200:
        with open(img_path, 'wb') as image_file:
            for chunk in resp:
                image_file.write(chunk)
#                tooter(title, selftext, post_id, img_path)
            return img_path
    else:
        print('[bot] Image failed to download. Status code: ' + str(resp.status_code))

def tooted(post_id):
    found = False
    with open('posted_posts.txt', 'r') as out_file:
        for line in out_file:
            if post_id in line:
                found = True
                break
    return found

def tooter(title, selftext, img_url, post_id):
    try:
        media = mastodon.media_post(image(img_url))
        mastodon.status_post(title + " " + selftext + "#historymemes", media_ids=media)
        with open('posted_posts.txt', 'a') as out_file:
            out_file.write(str(post_id) + '\n')
    except:
        print('err')

get()
