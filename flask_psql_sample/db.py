import psycopg2

def insert_car(car_model):
    ppsql = """INSERT INTO auto(car_model)
             VALUES(%s) RETURNING auto_id;"""
    conn = None
    car_id = None
    try:
        params = config()
        conn = psycopg2.connect("dbname='auto' user='postgres' host='localhost' password='123456'")
        cur = conn.cursor()
        cur.execute("""
            CREATE TABLE cars(
            id integer PRIMARY KEY,
            model text,
            name text,
            contractpath text
        )
        """)
        cur.execute(psql, (car_model,))
        auto_id = cur.fetchone()[0]
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return car_id
