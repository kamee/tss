from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_nav import Nav
from flask_nav.elements import *
from dominate.tags import img
import os
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
from flask import send_from_directory
from wtforms import Form, StringField, TextAreaField, validators, StringField, SubmitField
from wtforms.validators import DataRequired
from flask_wtf import FlaskForm
from wtforms import StringField, StringField, SubmitField
from wtforms.validators import DataRequired, Length
from wtforms import Form, BooleanField, StringField, PasswordField, validators

import db

class RegistrationForm(Form):
    model = StringField('model', [validators.Length(min=4, max=25)])
    doors = StringField('doors', [validators.Length(min=1, max=25)])
    year = StringField('year', [validators.Length(min=4, max=25)])
    price = StringField('price', [validators.Length(min=4, max=25)])


logo = img(src='./static/img/logo.png', height="50", width="50", style="margin-top:-15px")
#here we define our menu items
topbar = Navbar(logo,
    View('News', 'get_news'),
    View('Cars', 'get_cars'),
    View('Contracts', 'get_contracts'),
    View('Classement', 'get_classement'),
    View('Contact', 'get_contact'),
)

UPLOAD_FOLDER = '/home/hp/flas/'
ALLOWED_EXTENSIONS = {'txt', 'pdf'}

nav = Nav()
nav.register_element('top', topbar)

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'
Bootstrap(app)

def allowed_file(filename):
    return '.' in filename and \
          filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def send_data(name):
    print(name)

@app.route('/', methods=['GET'])
def get_index():
    return(render_template('index.html'))


@app.route('/news', methods=['GET'])
def get_news():
    return(render_template('news.html'))

@app.route('/cars', methods=["GET", "POST"])
def get_cars():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        model = form.model.data
        db.insert_car(model)
        print(model)
        flash('Thanks for adding')

    return(render_template('cars.html', form=form))

@app.route('/contracts', methods=["GET", "POST"])
def get_contracts():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file', filename=filename))
    return(render_template('contracts.html'))

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)


@app.route('/classement', methods=["GET"])
def get_classement():
    return(render_template('classement.html'))

@app.route('/contact', methods=["GET"])
def get_contact():
    return(render_template('contact.html'))

nav.init_app(app)

if __name__ == '__main__':
    app.run(debug=True)
