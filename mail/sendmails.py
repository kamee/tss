import smtplib
import email
import os.path
from string import Template
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase

class Message:
    MY_ADDRESS = ''
    PASSWORD = ''

    @staticmethod
    def get_contacts(filename):
        """
        Return two lists names, emails containing names and email addresses
        read from a file specified by filename.
        """

        # Variable contains all names in string value
        names = []
        emails = []
        with open(filename, mode='r', encoding='utf-8') as contacts_file:
            for a_contact in contacts_file:
                names.append(a_contact.split()[0])
                emails.append(a_contact.split()[1])
                return names, emails

    def main(self, plain_text):
        names, emails = self.get_contacts('mycontacts.txt')  # read contacts
        # set up the SMTP server
        s = smtplib.SMTP('smtp.gmail.com', 587)
        # s = smtplib.SMTP(host = 'smtp.mail.yahoo.com', port = 587)
        s.starttls()
        s.login(self.MY_ADDRESS, self.PASSWORD)

        for name, email in zip(names, emails):
            msg = MIMEMultipart()
            msg['From'] = self.MY_ADDRESS
            msg['To'] = email
            msg['Subject'] = "reporting"
            body = plain_text
            msg.attach(MIMEText(body, 'plain'))
            filename = "file.pdf"
            path = "filepath"
            attachment = open(path, "rb")
            p = MIMEBase('application', 'octet-stream')
            p.set_payload((attachment).read())
            encoders.encode_base64(p)
            p.add_header('Content-Disposition', "attachment; filename= %s" % filename)
            p.set_payload(attachment.read())
            encoders.encode_base64(p)
            p.add_header('Content-Disposition', "attachment; filename= %s" % filename)
            msg.attach(p)
            s = smtplib.SMTP('smtp.gmail.com', 587)
            s.starttls()
            s.login(self.MY_ADDRESS, self.PASSWORD)
            text = msg.as_string()
            s.sendmail(self.MY_ADDRESS, email, text)
            s.quit()


if __name__ == '__main__':
    m = Message()
    m.main()
