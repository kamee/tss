import json
import csv
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.common.by import By
import selenium.webdriver.support.ui as ui
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

def main():
    driver = webdriver.Firefox(executable_path="/home/fugio/.wdm/drivers/geckodriver/linux64/v0.29.0/geckodriver")
    driver.get("https://plans.nwleics.gov.uk/public-access/search.do?action=advanced")

    dates = driver.find_elements_by_class_name("date")
    element = driver.find_element_by_id("applicationValidatedStart").send_keys("01/01/2021")
    element = driver.find_element_by_id("applicationValidatedEnd").send_keys("31/01/2021")

    button = driver.find_element_by_class_name("button.primary").submit()

    try:
        scraping(driver)
        wait.until(lambda driver: driver.find_element_by_link_text('Next'))
        driver.find_element_by_link_text("Next").click()
    except NoSuchElementException:
        print("no element found")
        driver.close()

def scraping(driver):
    wait = ui.WebDriverWait(driver, 10)
    wait.until(lambda driver: driver.find_element_by_id('searchresults'))

    html_list = driver.find_element_by_id("searchresults")
    items = html_list.find_elements_by_tag_name("a")

    hrefs = []
    for elem in items:
        hrefs.append(elem.get_attribute("href"))

    details = []
    for href in hrefs:
        driver.get(href)
        data = {
            "url": href,
            "reference": driver.find_element_by_xpath("/html/body/div/div/div[3]/div[3]/div[3]/table/tbody/tr[1]/td").text,
            "address": driver.find_element_by_xpath("/html/body/div/div/div[3]/div[3]/div[3]/table/tbody/tr[5]/td").text,
            "application validated": driver.find_element_by_xpath("/html/body/div/div/div[3]/div[3]/div[3]/table/tbody/tr[4]/td").text,
            "status": driver.find_element_by_xpath("/html/body/div/div/div[3]/div[3]/div[3]/table/tbody/tr[7]/td").text,
            "proposal": driver.find_element_by_xpath("/html/body/div/div/div[3]/div[3]/div[3]/table/tbody/tr[6]/td").text
        }
        details.append(data)
        toCSV(details)

def toCSV(details):
    filename = "scraping.csv"
    header = ("url", "reference", "address", "validated", "status", "proposal")
    with open(filename, "w", newline="") as csvfile:
        wo = csv.writer(csvfile)
        wo.writerow(header)
        for i in details:
            wo.writerow(i.values())

main()
