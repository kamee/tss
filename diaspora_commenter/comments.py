import diaspy

signin = diaspy.connection.Connection(pod='POD_NAME',
                                username='YOUR_USERNAME',
                                password='YOURPASSWORD')
signin.login()

tag = "եսնորեկեմ"
text = """
    ողջոյն եւ բարի գալուստ,
    [դիասպորան](https://diasporafoundation.org/) ապակենտրոն եւ ազատ սոցիալական ցանց է,
    կազմուած միմեանցից անկախ, սակայն միմեանց հրապարակումներին հասանելիութիւն ունեցող հանգոյցներից։
    այն քեզ հնարաւորութիւն է տալիս լինել ազատ եւ ապահով։
    սփիւռքն այդ հանգոյցներից մեկն է։ սեփական հանգոյցը սակայն, քեզ կարող է դարձնել աւելի անկախ եւ ազատ։
    որոշ տեղեկութիւններ [ինչ է ազատ ծրագրակազմը](https://www.gnu.org/philosophy/free-sw),
    [ինչու է ապակենտրոնացումը կարեւոր](https://blog.joinmastodon.org/2018/12/why-does-decentralization-matter/)
    տեղեկութիւններ [տեղական հանգոյցների մասին](xn--y9aam3bxbanl3ck.xn--y9a3aq)։
    """

stream = diaspy.streams.Tag(signin, tag)
followTag = diaspy.streams.FollowedTags(signin)
followTag.add(tag)

checked = False
for post in stream:
    if checked == False:
        post.comment(text)
        checked = True

