import firestoreConfs
from datetime import datetime
import saveTo

startDay = '27'
endDay = '28'
days = []
days.append(startDay)
days.append(endDay)
allStores = {}
todayStores = {}

def check():
    if day in days:
        todayStores.update({ID: [payerName, priceUnit]})
        strStores = str(todayStores)
        saveTo.writePDF(strStores)

for doc in firestoreConfs.docs:
    ID = doc.to_dict()['store']
    payerName = doc.to_dict()['payerName']
    priceUnit = doc.to_dict()['priceUnit']
    createdDate = doc.to_dict()['created']
    allStores.update({ID: [payerName, priceUnit]})
    day = createdDate.strftime('%d')
    check()
