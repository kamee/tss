import firebase_admin
from firebase_admin import firestore
from firebase_admin import credentials

cred = credentials.Certificate('.json')
firebase_admin.initialize_app(cred, {
    'projectId': ''
    })

cloud = firestore.client()
users_ref = cloud.collection(u'payments')
docs = users_ref.stream()
