from fpdf import FPDF

def writePDF(strStores):
    """
    save firestoreData in the pdf file
    """
    pdf = FPDF()
    pdf.add_page()
    pdf.set_xy(0, 0)
    pdf.set_font('arial', 'B', 13.0)
    pdf.cell(ln=0, h=5.0, align='L', w=0, txt=strStores, border=0)
    pdf.output('firestoreData.pdf', 'F')
