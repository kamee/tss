import requestStatus

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

ref_req = db.reference('requests')
data = ref_req.get()

allKeys = data.keys()

phoneNumber = ""

def push():
    ref_push = db.reference('sms')
    postRef = ref_push.push()
    newPostChild = postRef.child('message')
    newPostChild.set(test_dict)

for keys in allKeys:
    specVal = data[keys]
    message = ""
    string = " "
    for specValKeys, specValValue in specVal.items():
        for item in str(specValValue):
            if isinstance(specValValue, dict):
                keysOfItems = ', '.join(specValValue.keys())
                message = keysOfItems + " "
            else:
                string += item
        text = message + " " + string
    test_dict = {'message': text, 'phone': phoneNumber}
    push()
