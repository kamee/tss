import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
from datetime import date
from firebase_admin import firestore

import xlsxwriter

from xlsxwriter import Workbook

cred = credentials.Certificate('.json')
firebase_admin.initialize_app(cred, {
  'projectId': '',
  'databaseURL': ''
})

ref = db.reference('sessions')
data = ref.get()
cloud = firestore.client()
users_ref = cloud.collection(u'users')
docs = users_ref.stream()
keys = data.keys()

myList = []

for key in keys:
    value = data[key]
    timestamp = value['startedAt']
    date_object = date.fromtimestamp(int(timestamp) / 1000).strftime('%Y-%m-%d %H:%m')
    dictionaries = {'userID': value['user'], 'time': date_object, 'mail': 'none', 'name': 'none'}
    for doc in docs:
        user = (doc.to_dict()['uid'])
        email = (doc.to_dict()['email'])
        name = (doc.to_dict()['display_name'])
    # there is bug
    if str(value['user']) == str(user):
        dictionaries.update({'userID': value['user'], 'time': date_object, 'name': name, 'mail': email})
        myList.append(dictionaries)
    else:
        dictionaries.update({'userID': value['user'], 'time': date_object})
        myList.append(dictionaries)

ordered_list=["time","userID","name","mail"]

wb=Workbook("new.xlsx")
ws=wb.add_worksheet("New Sheet")

first_row=0
for header in ordered_list:
    col=ordered_list.index(header)
    ws.write(first_row,col,header)

row=1
for it in myList:
    for _key,_value in it.items():
        col=ordered_list.index(_key)
        ws.write(row,col,_value)
    row+=1
wb.close()

