import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore


class getdata:
    # Fetch the service account key JSON file contents
    cred= credentials.Certificate('/home/user/Desktop/payva-internal/files/payvatest.json')


    firebase_admin.initialize_app(cred, {
        'projectId': 'payva-payment-test',
        'databaseURL': 'https://payva-payment-test.firebaseio.com',
        'authDomain': 'payva-payment-test.firebaseapp.com'
    })

    db = firestore.client()
    users_ref = db.collection(u'prices')
    docs = users_ref.stream()

    def firestore_data(self):
        for doc in self.docs:
            updated_key = doc.to_dict()['updated']
            # dates = updated_key.strftime('%d/%B/%Y:%H:%M')
            body = (u'{} => {}'.format(doc.id, doc.to_dict()))
            text = []
            text.append(body)
            return str(text)


if __name__ == '__main__':
    gd = getdata()
    gd.firestore_data()