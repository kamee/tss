from wtforms import SubmitField
from flask_wtf import FlaskForm


class ExportButton(FlaskForm):
    Export = SubmitField('Export')