import sys

sys.path.insert(0, '/home/user/PycharmProjects/getPriceData/ui/models')
sys.path.insert(0, '/home/user/PycharmProjects/getPriceData/')

from flask import Flask, request, render_template
from flask_bootstrap import Bootstrap
from export_button import ExportButton
# from users_details import UsersDetails
from getFirestoreData import getdata

app = Flask(__name__)
app.config.from_mapping(
    SECRET_KEY="123abc456")
Bootstrap(app)


class Export(ExportButton, getdata):
    @app.route('/', methods=['GET', 'POST'])
    def export():
        gd = getdata()
        forms = ExportButton(request.form)
        # if request.method == 'GET' and forms.validate_on_submit():
        if 'Export' in request.form:
            return gd.firestore_data()
        return render_template('registration.html', form=forms)


if __name__ == '__main__':
    app.run()
