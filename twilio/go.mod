module gitlab.com/kamee/twilio

go 1.14

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gofiber/fiber/v2 v2.25.0 // indirect
	github.com/lib/pq v1.10.4 // indirect
	github.com/sfreiberg/gotwilio v1.0.0 // indirect
)
