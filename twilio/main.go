package main

import (
  "bufio"
  "encoding/base64"
  "io/ioutil"
  "io"
  "os"
  "math/rand"
  "context"
  "fmt"
  "net/http"
  "time"
  "log"
  "encoding/xml"
  "database/sql"

  "github.com/sfreiberg/gotwilio"
)

const (
  accountSid = ""
  authToken = ""
)

const (
    username = "root"
    password = "root"
    hostname = "127.0.0.1:3306"
    dbname   = "twilio"
)

const (
  BASE_URL = "https://api.twilio.com/2010-04-01/Accounts/ACe203ab682f128346781bac689239821b" // accountSid
  FROM = "+16066719583"
)

type Response struct {
    XMLName xml.Name `xml:"Response"`
    Message string   `xml:"Message,omitempty"`
    To string `xml:"Message to"`
}

type SMSRequest struct {
    From string `form:"From"`
    // Body string `form:"Body"`
}

type UserConstruct struct {
  MySQL *sql.DB
  Department_ID string
  To_list []string
  Phonenumber string
  Message string
  Media64 string
}

func newUserConstruct(phonenumber, body, media string) *UserConstruct {
  return &UserConstruct{
    // To_list: toList,
    Phonenumber: phonenumber,
    Message: body,
    Media64: media,
  }
}

func initTestTwilioClient() *gotwilio.Twilio {
	return gotwilio.NewTwilioClient(accountSid, authToken)
}


func (u *UserConstruct) convertImage(url string) string {
   fileName :=  string(rand.Int())
   response, err := http.Get(url)
	 if err != nil {
     panic(err)
     return ""
	  }
	 defer response.Body.Close()

   if response.StatusCode != 200 {
     log.Fatal("fail with status code", response.StatusCode)

   }
   file, err := os.Create(string(fileName))
	 if err != nil {
     panic(err)
     return ""
   }
	 defer file.Close()

	 _, err = io.Copy(file, response.Body)
	 if err != nil {
     panic(err)
     return ""
	 }

  file_enc, _ := os.Open(fileName)
  reader := bufio.NewReader(file_enc)
  content, _ := ioutil.ReadAll(reader)

  encoded := base64.StdEncoding.EncodeToString(content)
  u.Media64 = encoded
  return u.Media64
}

func delete(ctx context.Context, sid string) {

    full_url := BASE_URL + "/Messages/" + sid + ".json"
    fmt.Println(full_url)
  	_, cancel := context.WithCancel(ctx)
  	req, _ := http.NewRequestWithContext(ctx, "DELETE", full_url, nil)
    req.SetBasicAuth(accountSid, authToken)
    req.Header.Add("Accept", "application/json")
    req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

    client := &http.Client{}
    go func() {
      time.Sleep(time.Second * 10)
  		cancel()
    }()

  	resp, err := client.Do(req)

  	if err != nil {
  		panic(err)
  	}

    if resp.StatusCode != 200{
      log.Fatal(resp.StatusCode)
    }
}

func (u *UserConstruct) getMessage(w http.ResponseWriter, req *http.Request){
      // err := req.ParseForm()
      // if err != nil{}
      // for keys, values := range req.Form {
      //   // for key, value := range values{
      //     fmt.Println(keys, values)
      //   // }
      // }


	req.ParseForm()
	from := req.FormValue("From")
  body := req.FormValue("Body")
  smsSid := req.FormValue("MessageSid")
  // mediaURL := req.FormValue("MediaUrl")

  u1 := UserConstruct{
    Message: body,
    Phonenumber: from,
    // Media64: u.convertImage(mediaURL),
  }

  autoResponse := Response{
    Message: "we got your message",
    // To: "",
  }

  text, err := xml.Marshal(autoResponse)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  Insert(u.MySQL, []UserConstruct{u1})

  w.Header().Set("Content-Type", "application/xml")
  w.Write(text)

  delete(context.Background(), smsSid)

  return

  // http.Redirect(w, req, "/sms", http.StatusFound)

}

func (u *UserConstruct) sendSMS(w http.ResponseWriter, r *http.Request){
    fmt.Println(u.To_list, u.Message)
    client := initTestTwilioClient()
    for i := 0; i < len(u.To_list); i++ {
      data, _, err := client.SendSMS(FROM, u.To_list[i], u.Message, "", "")
      if err != nil {
        fmt.Println(err)
      }

      recievedSid, _, err := client.GetMessage(data.Sid)
      if err != nil {
        log.Println(err)
      }
      if data.Status == "recieved" {
        delete(context.Background(), recievedSid.Sid)
      }
    }
    fmt.Fprintf(w, "sending sms...!")
}

func (u *UserConstruct) sendMMS(w http.ResponseWriter, r *http.Request){
  mediaUrl := []string{"https://pixy.org/src/20/201310.jpg"}
  client := initTestTwilioClient()
  for i := 0; i < len(u.To_list); i++ {
    data, _, err := client.SendMMS(FROM, u.To_list[i], u.Message, mediaUrl, "", "")
    if err != nil {
      fmt.Println(err)
    }
    recievedSid, _, err := client.GetMessage(data.Sid)
    if err != nil {
      log.Println(err)
    }
    if data.Status == "recieved" {
      delete(context.Background(), recievedSid.Sid)
    }
  }
  fmt.Fprintf(w, "sending mms...!")
}

func main() {
  user := &UserConstruct{}
  var message_body, answer, media_path string
  var size int

  db, err := DbConnection()
  user.MySQL = db

  if err != nil {
      log.Printf("Error %s when getting db connection", err)
      return
   }
   defer db.Close()
   log.Printf("Successfully connected to database")
   err = CreateUserTable(db)
   if err != nil {
       log.Printf("Create product table failed with error %s", err)
       return
   }

   if err != nil {
     log.Printf("Insert product failed with error %s", err)
     return
   }

  // insert(db)

  //     u1 := User{
  //         Phonenumber:  "+37498508610",
  //         Media64: "encoded64",
  //     }
  //
  //     u2 := User{
  //         Phonenumber: "+37494613398",
  //         Media64: "encoded",
  //     }
  //     // err = insert(db, u1)
  //     err = Insert(db, []User{u1, u2})
  //    err = UpdateMedia(db, []UserConstruct{u1, u2})

   fmt.Println("fetch...")
   numbers_list := Fetch(db)

   fmt.Println(numbers_list)
   for key, value := range(numbers_list){
     fmt.Println(key, value)
   }

   fmt.Print("how many nums: ")
   fmt.Scanln(&size)


   for i := 0; i < size; i++ {
      fmt.Printf("Enter id: ", i)
      fmt.Scanln(&answer)
      user.To_list = append(user.To_list, numbers_list[answer])
   }

   fmt.Print("enter your message: ")
   fmt.Scanln(&message_body)

   user.Message = message_body

   fmt.Print("enter your media path: ")
   fmt.Scanln(&media_path)

   user.Media64 = media_path
   if media_path != ""{
     user.convertImage(user.Media64)
   }

   http.HandleFunc("/get", user.getMessage)
   http.HandleFunc("/sms", user.sendSMS)
   http.HandleFunc("/mms", user.sendMMS)
   http.ListenAndServe(":8080", nil)
}
