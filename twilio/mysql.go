package main

import (
    "context"
    "database/sql"
    "fmt"
    "log"
    "time"
    "strings"

    _ "github.com/go-sql-driver/mysql"
)

func dsn(dbName string) string {
    return fmt.Sprintf("%s:%s@tcp(%s)/%s", username, password, hostname, dbName)
}

func DbConnection() (*sql.DB, error) {
    db, err := sql.Open("mysql", dsn(""))
    if err != nil {
        log.Printf("Error %s when opening DB\n", err)
        return nil, err
    }

    ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancelfunc()

    db, err = sql.Open("mysql", dsn(dbname))
    if err != nil {
        log.Printf("Error %s when opening DB", err)
        return nil, err
    }
    // defer db.Close()

    db.SetMaxOpenConns(20)
    db.SetMaxIdleConns(20)
    db.SetConnMaxLifetime(time.Minute * 5)

    ctx, cancelfunc = context.WithTimeout(context.Background(), 5*time.Second)
    defer cancelfunc()
    err = db.PingContext(ctx)
    if err != nil {
        log.Printf("Errors %s pinging DB", err)
        return nil, err
    }
    log.Printf("Connected to DB %s successfully\n", dbname)
    return db, nil
}

func CreateUserTable(db *sql.DB) error {
    query := `CREATE TABLE IF NOT EXISTS twilio_test(department_ID serial primary key, phonenumber text, media text)`
    ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancelfunc()
    res, err := db.ExecContext(ctx, query)
    if err != nil {
        log.Printf("Error %s when creating user table", err)
        return err
    }
    rows, err := res.RowsAffected()
    if err != nil {
        log.Printf("Error %s when getting rows affected", err)
        return err
    }
    log.Printf("Rows affected when creating table: %d", rows)
    return nil
}

func Insert(db *sql.DB, users []UserConstruct) error {
  fmt.Println("insert...")
    query := "INSERT INTO twilio_test(phonenumber, media) VALUES "
    var inserts []string
    var params []interface{}
    for _, v := range users {
        inserts = append(inserts, "(?, ?)")
        params = append(params, v.Phonenumber, v.Media64)
    }
    queryVals := strings.Join(inserts, ",")
    query = query + queryVals
    log.Println("query is", query)
    ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancelfunc()
    stmt, err := db.PrepareContext(ctx, query)
    if err != nil {
        log.Printf("Error %s when preparing SQL statement", err)
        return err
    }
    defer stmt.Close()
    res, err := stmt.ExecContext(ctx, params...)
    if err != nil {
        log.Printf("Error %s when inserting row into users table", err)
        return err
    }
    rows, err := res.RowsAffected()
    if err != nil {
        log.Printf("Error %s when finding rows affected", err)
        return err
    }
    log.Printf("%d users created simulatneously", rows)
    return nil
}

func Fetch(db *sql.DB) map[string]string {
  var numList = map[string]string{}
  res, err := db.Query("SELECT * FROM twilio_test")

   defer res.Close()

   if err != nil {
       panic(err)
   }

   for res.Next() {
       var phonenumber, department_ID, media string
       err := res.Scan(&department_ID, &phonenumber, &media)
       fmt.Println(department_ID)
       if err != nil {
           panic(err)
       }
       numList[department_ID] = phonenumber
   }
    return numList
}

func UpdateMedia(db *sql.DB, users []UserConstruct) error {
  query := "UPDATE twilio_test set media VALUES "
  var inserts []string
  var params []interface{}
  for _, v := range users {
      inserts = append(inserts, "(?)")
      params = append(params, v.Media64)
  }
  queryVals := strings.Join(inserts, ",")
  query = query + queryVals
  log.Println("query is", query)
  ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
  defer cancelfunc()
  stmt, err := db.PrepareContext(ctx, query)
  if err != nil {
      log.Printf("Error %s when preparing SQL statement", err)
      return err
  }
  defer stmt.Close()
  res, err := stmt.ExecContext(ctx, params...)
  if err != nil {
      log.Printf("Error %s when inserting row into users table", err)
      return err
  }
  rows, err := res.RowsAffected()
  if err != nil {
      log.Printf("Error %s when finding rows affected", err)
      return err
  }
  log.Printf("%d users updated simulatneously", rows)
  return nil
}
