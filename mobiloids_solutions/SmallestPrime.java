import java.util.*;

class SmallestPrime {
	public static boolean isPrime(int n){
		if(n <= 1){
			return false;
		}

		for(int i = 2; i < n; i++){
			if(n % i == 0){
				return false;
			}
		}

		return true;
	}

    public static int solution(int[] a) {

        HashSet<Integer> hashSet = new HashSet<Integer>();
	int smallInt = 1;

        for (int i = 0 ; i < a.length; i++) {
	            hashSet.add(a[i]);                     
	}

	 for(int i = 0; i < a.length; i++){
	 	if(!hashSet.contains(smallInt) && isPrime(smallInt)){
			return smallInt;
		}

		smallInt++;
	 }

        return -1;
    }

	public static void main(String[] args){
		int[] array = {1, 2, 3, 4, 5, 10, 13, 12};
		System.out.println(solution(array));
	}
}
