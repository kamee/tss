public class Tetris{
	public static boolean rowColEquals(int[] row) {
	        for (int i = 1; i < row.length; i++) {
	                if (1 != row[i])
		                return false;
		        }
	        return true;
        }

	static void moveDown(int[][] board, int height, int width){
		for(int i=0; i < height; i++){
			for(int j=0; j < width; j++){
				 if (!rowColEquals(board[i])) {
					 System.out.print(board[i][j] + " ");
				 }
			}
			System.out.println();
		}
	}

	public static void main(String[] args){
		int[][] board = {
			{0, 0, 0, 0, 0},
			{0, 0, 0, 1, 0},
			{0, 0, 1, 1, 1},
			{1, 1, 1, 1, 1},
			{0, 0, 1, 1, 1},
			{1, 1, 1, 1, 1}
		};
		moveDown(board, 6, 5);
	}
}
