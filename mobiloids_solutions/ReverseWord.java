import java.util.*;

public class ReverseWord{

	public static String reverseString(String s){
		char ch[]=s.toCharArray();  
		String revString = "";  

		for(int i=ch.length-1;i>=0;i--){  
			revString += ch[i];  
		}
		
		return revString;  
	}

	public static int reversePairsCount (List<String> words) {
		int count = 0;

		for (int i = 0; i < words.size(); i++) {
			for (int k = i + 1; k < words.size(); k++) {
				if(words.get(i).equals(reverseString(words.get(k)))){
					count++;
				}
			}
		}

		return count;
	}

	public static void main(String[] args){
		 List<String> words=new ArrayList<String>();  
		 words.add("top");
		 words.add("aaa");
                 words.add("read");
                 words.add("stop");
		 words.add("aaa");
		 words.add("table");
		 words.add("pots");
		 words.add("pot");
		 System.out.println(reversePairsCount(words));
	}



}
